import socket
import json
import pandas 

PORT = 1100
SERVER = socket.gethostname()
ADDR = (SERVER,PORT)
CHUNK_SIZE = 1024

client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

def main():
    while True:
        print('1. To get mails given date\n2. To get sent mails given employee id\n3. To get received mail given employee id \n4. To send mail\n5. To terminate the connection')
        choice = input('Enter your choice: ')
        if choice == '1':
            inputDate = input('Enter the Date(yyyy-mm-dd): ')
            client.send(json.dumps({'op': 1, 'date': inputDate}).encode('utf-8'))
            res = json.loads(client.recv(CHUNK_SIZE).decode('utf-8'))['data']
            res = pandas.read_json(res)
            print(res)
        elif choice == '2':
            empID = input('Enter the employee id: ')
            client.send(json.dumps({'op': 2, 'empID': empID}).encode('utf-8'))
            res = json.loads(client.recv(CHUNK_SIZE).decode('utf-8'))['data']
            res = pandas.read_json(res)
            print(res)
        elif choice == '3':
            empID = input('Enter the employee id: ')
            client.send(json.dumps({'op': 3, 'empID': empID}).encode('utf-8'))
            res = json.loads(client.recv(CHUNK_SIZE).decode('utf-8'))['data']
            res = pandas.read_json(res)
            print(res)
        elif choice == '4':
            senderEmpId = input('Enter sender employee id: ')
            receiverEmpId = input('Enter receiver employee id: ')
            subject = input('Enter the subject: ')
            content = input('Enter the content\n')
            client.send(json.dumps({'op': 4, 'senderEmpId': senderEmpId, 'receiverEmpId': receiverEmpId, 'subject': subject, 'content': content}).encode('utf-8'))
            res = json.loads(client.recv(CHUNK_SIZE).decode('utf-8'))['data']
            print(res)
        elif choice == '5':
            client.send(json.dumps({'op': 5}).encode('utf-8'))
            client.close()
            return
        else:
            print('Invalid Input')

try:
    client.connect(ADDR)
    main()
except:
    print('Connection Lost')