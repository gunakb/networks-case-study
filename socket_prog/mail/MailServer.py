import socket
import threading
import json
import pandas
import datetime 
import random

fileName = 'mailserver.csv'

def addData():
    date = []
    time = []
    senderId = []
    receiverId = []
    subject = []
    content = []
    startDate = datetime.date(2000, 1, 1)
    endDate = datetime.date.today()
    timeBetweenDates = endDate - startDate
    daysBetweenDates = timeBetweenDates.days
    for i in range(10):
        randomNumberDays = random.randrange(daysBetweenDates)
        randomDate = startDate + datetime.timedelta(days = randomNumberDays)
        hh = random.randrange(0, 24)
        mm = random.randrange(0, 60)
        ss = random.randrange(0, 60)
        date.append(randomDate)
        time.append(datetime.time(hh, mm, ss))
        senderId.append('M' + str(100 + 2*i))
        receiverId.append('M' + str(100 + 2*i + 1))
        subject.append(senderId[i])
        content.append('from ' + senderId[i] + ' to ' + receiverId[i])
    mailDf = pandas.DataFrame({'Date': date, 'Time': time, 'Sender ID': senderId, 'Receiver ID': receiverId, 'Subject': subject, 'Content': content})
    mailDf.to_csv(fileName, index = False)

def getMailsDate(inputDate):
    mailDf = pandas.read_csv(fileName)
    res = mailDf.loc[mailDf['Date'] == inputDate].to_json()
    return json.dumps({'data': res}) 

def getSentMails(empId):
    mailDf = pandas.read_csv(fileName)
    res = mailDf.loc[mailDf['Sender ID'] == empId].to_json()
    return json.dumps({'data': res})

def getReceivedMails(empId):
    mailDf = pandas.read_csv(fileName)
    res = mailDf.loc[mailDf['Receiver ID'] == empId].to_json()
    return json.dumps({'data': res})

def sendMail(senderId, receiverId, subject, content):
    mailDf = pandas.read_csv(fileName)
    currentDate = datetime.date.today()
    now = datetime.datetime.now()
    currentTime = now.strftime("%H:%M:%S")
    mailDf.loc[len(mailDf.index)] = [currentDate, currentTime, senderId, receiverId, subject, content]
    mailDf.to_csv(fileName, index = False)
    return json.dumps({'data': 'Mail Sent'})

def handleClient(client, address):
    while True:
        message = client.recv(CHUNK_SIZE).decode('utf-8')
        if not message:
            continue
        message = json.loads(message)

        if(message['op'] == 1):
            client.send(getMailsDate(message['date']).encode('utf-8'))
        elif(message['op'] == 2):
            client.send(getSentMails(message['empID']).encode('utf-8'))
        elif(message['op'] == 3):
            client.send(getReceivedMails(message['empID']).encode('utf-8'))
        elif(message['op'] == 4):
            client.send(sendMail(message['senderEmpId'], message['receiverEmpId'], message['subject'], message['content']).encode('utf-8'))
        elif(message['op'] == 5):
            client.close()
            return
try:
    with open(fileName,'r') as csvFile:
        pass
except:
    with open(fileName,'a') as csvFile:
        addData()

PORT = 1100
SERVER = socket.gethostname()
ADDR = (SERVER,PORT)
CHUNK_SIZE = 1024

server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server.bind(ADDR)

server.listen()
print('Server At : ',ADDR)
while True:
    client, address = server.accept()
    thread = threading.Thread(target = handleClient, args = (client, address))
    thread.start()