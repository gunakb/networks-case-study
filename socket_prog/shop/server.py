import csv
import socket
import threading
import json

from shopping import ShoppingDepartment

def clientIndividual(client,address):
    while True:
        message = client.recv(CHUNK_SIZE).decode('utf-8')
        message = json.loads(message)

        shopping = ShoppingDepartment()
        try: 
            if(message['op'] == 7):
                client.send(shopping.addProduct(message['product']).encode('utf-8'))
            
            elif(message['op'] == 8):
                client.send(shopping.buyProduct(message['pid'],message['qty']).encode('utf-8'))

            elif(message['op'] == 9):
                client.send(shopping.updatePrice(message['price'],message['pid']).encode('utf-8'))

            elif(message['op'] == 10):
                client.send(shopping.updateQty(message['qty'],message['pid'],).encode('utf-8'))

            elif(message['op'] == 11):
                client.send(shopping.removeOutOfStock().encode('utf-8'))

            elif(message['op'] == 12):
                client.send(shopping.displayAllProducts().encode('utf-8'))

            elif(message['op'] == 20):
                client.send(json.dumps({"data" : ["Connection Closed"]}).encode('utf-8'))
                client.close()
                return

            else:
                client.send(json.dumps({"data" : ["Invalid Operation"]}).encode('utf-8'))
        except:
            client.send(json.dumps({'data' : ["Bad Request"]}).encode('utf-8'))       




#Createfile if it doesnt exist
try:
    with open('shopping.csv','r') as csvFile:
        pass
except:
    with open('shopping.csv','a') as csvFile:
        csvWriter = csv.writer(csvFile)
        fields = ['pid','Name','Qty','Price','Seller']
        csvWriter.writerow(fields)



PORT = 1101
SERVER = socket.gethostname()
ADDR = (SERVER,PORT)
CHUNK_SIZE = 1024

server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server.bind(ADDR)

server.listen()
print('Server At : ',ADDR)
while True:
    client, address = server.accept()
    thread = threading.Thread(target = clientIndividual, args = (client,address))
    thread.start()


