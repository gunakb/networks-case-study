import pandas as pd
import csv
import json

class ShoppingDepartment:
    
    def addProduct(self,productList):
        with open('shopping.csv','a') as csvFile:
            csvWriter = csv.writer(csvFile)
            csvWriter.writerow(productList)
        return json.dumps({'data' : ['Added New Product']})

    def updatePrice(self,price,productId):
        df = pd.read_csv('shopping.csv')
        df.loc[df['pid'] == productId, 'Price'] = price
        df.to_csv('shopping.csv',index = False)
        return json.dumps({'data' : ['Updated Price']})

    def updateQty(self,qty,productId):
        df = pd.read_csv('shopping.csv')
        df.loc[df['pid'] == productId, 'Qty'] += qty
        df.to_csv('shopping.csv',index = False)
        return json.dumps({'data' : ['Updated Quantity']})

    def buyProduct(self,productId,boughtQty):
        df = pd.read_csv('shopping.csv')
        qty = df.loc[df['pid'] == productId, 'Qty'].item()
        if(qty >= boughtQty):
            df.loc[df['pid'] == productId, 'Qty'] -= boughtQty
            df.to_csv('shopping.csv',index = False)
        return json.dumps({'data' : [df.loc[df['pid'] == productId, 'Price'].item()] })
    
    def removeOutOfStock(self):
        df = pd.read_csv('shopping.csv')
        toBeDelted = df[df['Qty'] == 0].index
        while(len(toBeDelted)):
            df.drop(toBeDelted , inplace=True)
            toBeDelted = df[df['Qty'] == 0].index
        df.to_csv('shopping.csv',index = False)
        return json.dumps({'data' : ['Removed Out Of Stock']})
    
    def displayAllProducts(self):
        data = []
        with open('shopping.csv','r') as csvFile:
            csvReader = csv.reader(csvFile)
            next(csvReader)
            for row in csvReader:
                data.append(row)
            
        return json.dumps({'data' : data})


            


        


    
