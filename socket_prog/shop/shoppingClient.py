import json
import socket


PORT = 1101
SERVER = socket.gethostname()
ADDR = (SERVER, PORT)
CHUNK_SIZE = 10240

client = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
client.connect(ADDR)

while True:
    print('\nEnter \n 7: Add Product \n 8: Buy Product \n 9: Update Price \n 10: Update Qty \n 11: Remove Out Of Stock \n 12:Display All Products')
    choice = int(input("Choice:"))
    
    if(choice == 7):
        pid = input('Enter Pid:')
        Name = input('Enter Name:')
        Qty = int(input('Enter Qty:'))
        Price = int(input('Enter Price:'))
        Seller = input('Enter Seller:')

        data = {
        'op' : choice,
        'product' : [pid, Name, Qty, Price, Seller] 
        }
    
    elif(choice == 8):
        pdt = input('Enter Product id:')
        qty = int(input('Qty:'))
        data = {
            'op' : choice,
            'pid' : pdt,
            'qty' : qty
        }

    elif(choice == 9):
        pdt = input('Enter Product id:')
        price = int(input('Price:'))
        data = {
            'op' : choice,
            'pid' : pdt,
            'price' : price
        }

    elif(choice == 10):
        pdt = input('Enter Product id:')
        qty = int(input('Enter extra Qty:'))
        data = {
            'op' : choice,
            'pid' : pdt,
            'qty' : qty
        }

    elif(choice == 11):
        data = {
            'op' : choice,
        }

    elif(choice == 12):
        data = {
            'op' : choice,
        }
    
    else:
        data = {
            'op' : choice,
        }

    
    message = json.dumps(data)
    client.send(message.encode('utf-8'))
    
    msg = client.recv(CHUNK_SIZE).decode('utf-8')
    msg = json.loads(msg)
    for row in msg['data']:
        print(row)