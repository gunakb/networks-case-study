import socket
import threading
import json
from numpy.core.fromnumeric import sort
import pandas as pd
import random
from datetime import datetime

file='ott.csv'

PORT=1111
SERVER = socket.gethostname()
ADDR = (SERVER,PORT)
CHUNK_SIZE = 1024
try:
    ott=pd.read_csv(file)
except:
    empty={
        'name': [],
        'language': [],
        'genre': [],
        'id': []
    }
    ott=pd.DataFrame(empty)
    ott.to_csv(file,index=False)

def handleClient(client, address):
    while True:
        message = client.recv(CHUNK_SIZE).decode('ascii')
        if not message:
            continue
        message = json.loads(message)
        if message['op']==1:
            client.sendall(listAll().encode('ascii'))
        elif message['op']==4:
            try:
                addMovie(message)
                client.sendall(json.dumps({"success":"added"}).encode('ascii'))
            except OSError as err:
                print("OS error: {0}".format(err))
                client.sendall(json.dumps({"error":"try again"}).encode('ascii'))
        elif message['op']==2:
            client.sendall(listOnLang(message['language']).encode('ascii'))
        elif message['op']==3:
            client.sendall(listOnGen(message['genre']).encode('ascii'))
        else:
            client.close()
def listAll():
    ott=pd.read_csv(file)
    return ott.to_json()

def listOnLang(language):
    ott=pd.read_csv(file)
    return ott[ott['language']==language].to_json()
def listOnGen(genre):
    return ott[ott['genre'].str.contains(genre)].to_json()

def addMovie(movie):
    ott=pd.read_csv(file)
    random.seed()
    movie['id']=int(random.random()*1000)
    movie.pop('op')
    print("To set: ",movie)
    ott.loc[len(ott.index)]=list(movie.values())
    print(ott)
    ott.to_csv(file,index=False)

server=socket.socket()
server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
server.bind(ADDR)
server.listen()
print('Server Listening at : ',ADDR)
while True:
    client, address = server.accept()
    thread = threading.Thread(target = handleClient, args = (client, address))
    thread.start()