import socket
import json
import pandas as pd

def listAll():
    toSend={
        'op':1
    }
    client.send(json.dumps(toSend).encode('ascii'))
    response=client.recv(CHUNK_SIZE).decode('ascii')
    response=pd.read_json(response)
    print(response)

def listOnLang(language):
    toSend={
        'op':2,
        'language':language
    }
    client.send(json.dumps(toSend).encode('ascii'))
    response=client.recv(CHUNK_SIZE).decode('ascii')
    try:
        response=pd.read_json(response)
    except OSError as err:
        print("OS error: {0}".format(err))
        response="Error! Try again with correct input"
    print(response)
def listOnGen(genre):
    toSend={
        'op':3,
        'genre':genre
    }
    client.send(json.dumps(toSend).encode('ascii'))
    response=client.recv(CHUNK_SIZE).decode('ascii')
    response=pd.read_json(response)
    print(response)

def addMovie(movie):
    toSend={
        'op':4,
        'name':movie['name'],
        'language':movie['language'],
        'genre': movie['genre']
    }
    client.sendall(json.dumps(toSend).encode('ascii'))
    print("sent")
    response=json.loads(client.recv(CHUNK_SIZE).decode('ascii'))
    print(response)



PORT=1111
SERVER = socket.gethostname()
ADDR = (SERVER,PORT)
CHUNK_SIZE = 1024

client = socket.socket()
try:
    client.connect(ADDR)
    print("Welcome to the OTT!")
    flag=True
    while flag:
        print("\n\nEnter the choice:\n1 - List All\n2 - List Based on language\n3 - List based on Genre\n4 - Add a movie\n5 - Exit\n\n")
        choice=input("Choice number: ")
        if choice=='1':
            listAll()
        elif choice=='2':
            print("List Based on Language")
            language=input("Enter the Language (as per ISO 369-3): ")
            listOnLang(language)
        elif choice=='3':
            print("List Based on Genre")
            genre=input("Enter the genre: ")
            listOnGen(genre)
        elif choice=='4':
            print("Add a new movie")
            name=input("Name: ")
            language=input("Language (as per ISO 369-3): ")
            genre=input("Genre(s) (with space, if many) :")
            movie={
                'name':name,
                'language':language,
                'genre':genre
            }
            addMovie(movie)
        elif choice=='5':
            print("Bye!!")
            client.close()
            flag=False

        else:
            print("Error! Bad choice: {}\n Enter a correct choice number (1-4)".format(choice))    
    
except OSError as err:
    print("OS error: {0}".format(err))
